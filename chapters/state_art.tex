%!TEX root = ../thesis.tex
%%--------------------------------------------------------------------------
%% State of the art
%%--------------------------------------------------------------------------


Location-aware mobile apps in the past years were mainly focused on outdoor environments due to the built-in technologies smartphones had in that period. In particular the GPS sensor was the most used in these apps for this purpose: from road navigation systems to activity tracking applications it was able to fulfill all their needs.\\
Now we are living an evolution of these smart applications bringing their capabilities indoor: proximity marketing, home automation and indoor navigation are only few of the fields in which this capabilities are required.\\
This chapter describes the evolution in indoor-location, the currently adopted solutions and the ones available in the near future.\\
The first section describes several approaches used in last years from the technological point of view, with a brief evolution of its usage over time.\\
The second section examines solutions currently available in commercial products and the platform supporting them, with a brief description of the additional features offered.\\


\section{Technological overview}
\label{sec:tech_overview}
In this section we'll see an overview of the currently available technologies for indoor location purposes and some projects where these technologies were used.

\subsection{WiFi}
\label{sub:wifi}
The first experiments in indoor location field were based on wifi networks. The main advantage using wifi is the broad diffusion of this technology in most of interesting environments like offices, homes and several city areas.\\
In wifi-based location projects, three main approaches have been taken into account and we want to describe them below.

\subsubsection{Fingerprinting}
\label{subsub:tech_fingerprint}
The idea behind fingerprint is mainly based on having a ``discrete environment model" from the location point of view: this means that the whole environment considered for indoor-location activity is divided into predefined areas, for example a $3*3$ meters square: the systems using this technique most of the times return location results based on the aforementioned discrete model instead of a continuous one. For example in Redpin project's tests\cite{redpin} when the user queries the system for his own location the system returns the room number having most similar fingerprints.\\
A fingerprint is usually composed by a tuple of signals parameters (such as network identifiers, signal strength, and access point information) from different access points in order to map this fingerprint with the real location where data were measured.\\
Most of the systems maintain a database with the set of all fingerprints measured for each ``discrete area" in order to compute the similarity between user measures and stored ones and return a location best estimate.\\
The deployment of the application consists of two phases: a measurement phase where several measurements are performed, for each area in the environment, in order to detect and store these fingerprints and their maps with areas, and a second phase where the running applications periodically computes the fingerprint resulting by consequent scans and look for a match on the previous phase mapping; if it finds a match the mapped real world area is considered the actual user/device location.\\
This technique led to poor results mainly due to WiFi RSSI measurements deviation: for example in case of the Redpin\cite{redpin} project the requirement was to have room-level accuracy (where room's walls mitigate measurements variations over time) using WiFi, Bluetooth and GSM network fingerprints.

\subsubsection{RSSI}
\label{subsub:rssi}
In this case the idea is to maintain a list of the wifi APs in the considered environment with their own respective absolute location coordinates. Each device detects several signal strength levels from near APs and then uses a trilateration algorithm in order to identify its real absolute location.\\
As shown in \cite{rssi_algorithm} an algorithm has an average deviation of 0.85m considering an $8*7$ m$^2$ room with 3 APs inside.\\
The main problem in this approach is the signal path loss model that should take into account several possible scenarios: a line of sight between signal transmitters and receivers, or an interrupted path due to obstacles presence and also signals reflection caused by walls or objects surrounding both receivers and transmitters.

\subsubsection{TDoA}
\label{subsub:tdoa}
In the idea of TDoA\cite{tdoa} (Time Difference of Arrival), the position of an object is calculated by measuring the time taken from a packet being sent from a transmitter device to being received by a receiver. By means of Wi-Fi, a client transfers out a time embossed signal which is received by the APs. From the dissimilarity in time between sending and receiving the signals, the distance between the AP and the client can be designed. When 3 access points are been used, by the principle of triangulation, it's possible to establish location of a client to an accuracy of less than 5 meters\cite{wifi_location}.

\subsection{Radio Frequency - Ultrasonic signals}
\label{sub:radio_frequency}
Radio frequency and ultrasonic signals were used in first indoor location approaches: projects using them are mainly older ones because smartphones were not broadly diffused or they had limited capabilities, forcing the use of dedicated hardware to attach to mobile entities tracked.\\
In last months some projects as Google Nearby\cite{google_nearby} brought it back to the front in proximity communication field thanks to newer smartphones computational capabilities: as explained better in the following subsection, Nearby uses an inaudible sound to transmit small data packets between devices; it exploits the fact that if two devices can exchange a valid data packet in this way, then they are one next to the other.


\subsection{Bluetooth}
\label{sub:bluetooth}
Bluetooth is the mostly used technology in indoor-location field because of its broad diffusion on everyday life devices (such as smartphones, tablets, cars, etc.) and the interesting features offered by its latest implementations: for example Bluetooth 4 introduces new low energy capabilities, allowing new power and interaction modes and a service-based architecture through the attribute protocol (ATT).\\
Furthermore, the low cost of hardware platforms makes solutions based on Bluetooth the preferred ones for commercial applications.

\subsubsection{First experiments}
\label{subsub:bt_first_experiments}
Some old experiments such as the one reported in Hallberg's work\cite{positioning_bt} started using 
as beacon whatever Bluetooth-enabled device, from PC keyboard adapters to mobile phones. This method uses bluetooth connection's RSSI in order to compute the mobile unit's position (which is the information they wanted) through the distance from the beacon and beacon's position stored by a ``positioning service". In this paper they reached an average position error of 1.7m +/- 1.7m.\\
Several applications were implemented on top of similar architectures (or even simpler ones, like exploiting only device to device detection), where distance accuracy was not so crucial as for other applications: applications like MobiTip\cite{mobitip} (a mobile proximity based recommender system) or social centered applications, usually need raw proximity information.\\
In latest years the ``beacons approach" has shown to be successful in lots of application fields, and some companies, like Apple, made first steps in this field.

\subsubsection{Bluetooth Smart and beacons}
\label{subsub:bluetooth_smart}
In 2010 Bluetooth version 4.0 was released and then completed in 2011 with the 4.1 version release. It introduced new features like a service-based architecture through the attribute protocol (ATT) and new power and interaction modes through Bluetooth Smart\cite{bluetooth_smart} (also known as Bluetooth Low Energy or BLE) specification: in particular low-energy mode and advertising communication mode are interesting for Bluetooth-powered beacons.\\
Bluetooth Smart beacons are Bluetooth Smart compliant devices and they act as packets emitter (see various formats in iBeacon - \ref{subsub:tech_ibeacon}, UriBeacon - \ref{subsub:tech_uribeacon} and Eddystone - \ref{subsub:tech_eddystone} sections): in fact, using low-energy features, they can periodically broadcast data packets that Bluetooth Smart (or Smart Ready) devices can listen\cite{bluetooth_smart} and use to perform context-specific actions; the advertising-only and low-power modes allow a beacon to have a really long battery life (even years in some cases). Furthermore smartphones can scan for advertised packets (also in a low-power manner) without the need to pair to transmitters, enabling ambient discovery tasks that is a key feature for BLE beacons in context-aware applications.\\
The last features contributing to their broad diffusion is the low hardware cost they have: the average price is in a 5-40\$ range depending on vendors, transmission power/range and battery life characteristics.


\subsubsection{iBeacon}
\label{subsub:tech_ibeacon}
Apple was maybe the first making a move in the Bluetooth Low Energy beacons field developing a standard protocol called iBeacon that defines the broadcasted messages format: in particular it advertises a structured identifier that iBeacon compatible applications can use for trigger context-specific actions such as running background task, showing user notifications or launching apps.\\

%Figura con schema pacchetto iBeacon
\begin{figure}[H]
	\centering{\includegraphics[width=\textwidth]{images/state_art/ibeacon-packet.png}}
	\caption{iBeacon packet schema}
	\label{fig:ibeacon_packet_schema}
\end{figure}
As shown in Figure \ref{fig:ibeacon_packet_schema}, the broadcasted packet composes of several fields; we will examine the PDU Data chunk:
\begin{itemize}
	\item iBeacon prefix: this field contains some information about the beacon itself, such as the company code, or about the packet such as the broadcasted packet type (in case of iBeacon packets last 2 bytes have fixed value 0x0215)
	\item Proximity UUID: is the beacon's main identifier that most of BLE beacons libraries use to filter out detected beacons
	\item Major: is an identifier used to group beacons with the same UUID
	\item Minor: is an identifier used to identify individual beacons
	\item TX Power: is the strength of the signal measured at 1 meter from the iBeacon. This number is then used to determine how close you are to the iBeacon
\end{itemize}

An example could clarify the identifiers hierarchy: if Apple decides to deploy some beacons in each of its stores, it would define a unique UUID all beacons in all stores all around the world will broadcast; this will allow Apple devices to know when they entered an Apple store. In order to identify which Apple store they entered, the same major value would be broadcasted by all beacons in the same store. Finally, if the app would understand where the device is inside the store, all beacons in all stores would broadcast the same Minor value for the same location in the store.

\subsubsection{UriBeacon}
\label{subsub:tech_uribeacon}
UriBeacon\cite{uri_beacon_spec} is an open format specification for BLE broadcast packets. The project started in 2014 and its goal was to define a new way to discover nearby smart things  and give a ``web presence" to each of them (an IoT-oriented approach), proposing itself as a bridge between Open Web technologies and Bluetooth beacons.\\
A UriBeacon can broadcast both HTTP URLs (http and https), UUID URNs (128-bit universally unique identifiers) as well as other URIs.

\subsubsection{Eddystone}
\label{subsub:tech_eddystone}
Recently Google stepped into BLE beacons world with its own new open standard (available under the open-source Apache v2.0 license) called Eddystone (a kind of UriBeacon successor). As reported from Eddystone project's website \textit{``Eddystone is a protocol specification that defines a Bluetooth low energy message format for proximity beacon messages. It describes several different frame types that may be used individually or in combinations to create beacons that can be used for a variety of applications."}\\
Respect to iBeacon this standard is more focused on broadcasting location information to nearby users (as beacon's latitude and longitude) and it's specifically designed to easily integrate applications in Google environment (using Google Nearby related services).\\
Furthermore Eddystone includes an ad-hoc module for beacons fleet management, called Proximity Beacons API: this API allows people who maintain beacons powered applications to manage large fleet of beacons in a centralized manner; this option enforces our beliefs in terms of beacons diffusion in the near future.\\
Eddystone differs from iBeacon because it's an open standard and because it allows its devices to broadcast three different frame types where iBeacon had a single frame type broadcasting an UUID, a major and minor number.\\

In Figure \ref{fig:eddystone_packet_schema} you can see Eddystone packets schema. Here we report a brief description of Eddystone's frame types from specification\cite{eddystone_spec}:
\begin{itemize}
	\item \textbf{Eddystone-UID}: it's the iBeacon parallel frame but it broadcasts a 16byte identifier where 10 bytes act as UUID (called namespace) and 6 bytes as instance (similar to major and minor in iBeacon)
	\item \textbf{Eddystone-URL}: this frame broadcasts a URL using a compressed encoding format in order to fit more within the limited advertisement packet, using the encoded URL to access remote resource stored in Google's cloud services.
	\item \textbf{Eddystone-TLM}: this frame broadcasts telemetry information about the beacon itself such as battery voltage, device temperature, and counts of broadcasted packets.
\end{itemize}

%Figura con schema pacchetto Eddystone
\begin{figure}[H]
	\centering{\includegraphics[width=\textwidth]{images/state_art/eddystone_packet}}
	\caption{Eddystone packet schema}
	\label{fig:eddystone_packet_schema}
\end{figure}
Google will include Eddystone support since Android Marshmallow 6.0 and will release an SDK for iOS devices in order to allow developers to create Eddystone powered apps: in fact Eddystone can be used in a ``standalone" manner, as with iBeacon, listening for nearby beacons and react to their proximity on the basis of UID frames; anyway, its power is shown in Google environments and open source projects such as Physical web\cite{physical_web}.

\subsection{Geo-Magnetism sensing}
\label{sub:tech_geomagnet}
Some studies reported quite accurate results obtained in indoor environments location using geo-magnetism sensors.\\
The paper Indoor Location Sensing Using Geo-Magnetism\cite{geo_magnetic} describes this approach: in brief this method exploits geomagnetic field distortions caused by steel and concrete skeletons of buildings.\\
Paper authors investigated the magnetic-field inside a building in order to map magnetic field distortion. Using an electronic compass mounted onto a rotating robotic arm, they moved along a straight line (a corridor) and for each position they measured heading values; they stored these values with the corresponding real headings. They noticed that the heading error varied along the corridor, so they suspected that the error was due to the building's structure.\\
The proposed system uses a fingerprint technique (see section \ref{subsub:tech_fingerprint}) where a pre-deployment phase is performed in order to map indoor locations' geomagnetic records with their corresponding real environment location. The clients, collecting the signature during their run, ask for a fingerprint match with a real location to the server that eventually reports back the matched location. In this system they used specific hardware with size constraints similar to smartphones size.\\
The mean error (of the paper proposed system) reported in case of atriums with radius greater than 15m is about 3m.\\
The limitation of the system is that the chance of error increases with the size of the fingerprint map, and the cost of mapping every space in a building may require significant effort and time using the presented method.\\
The advantages respect to other indoor-location systems like beacons-based (either WiFi or Bluetooth) is the lower deployment cost especially in large buildings and the immediate total covering of the building (obviously after the mapping phase). 



\subsection{LTE Direct}
\label{sub:tech_lte}
LTE is mainly known as a new wireless technology for data exchange but it’s also expanding into new frontiers as proved by its evolutions like LTE Advanced and LTE Direct\cite{lte_direct_qualcomm}.\\
LTE Direct uses the LTE licensed spectrum and is part of the Release 12 of the 3GPP standard. It is particularly interesting by the proximity point of view because it aims to provide an ``always-on device-to-device proximity service" that scales on large number of devices, all in a distributed fashion. LTE Direct proposes itself as a reference technology to overcome main limits of current proximity services approaches using Bluetooth beacons or geolocation: in fact they either use centralized architectures (e.g. in location-based approaches) with privacy issues due to user tracking data, or have a small working range (e.g in case of Bluetooth beacons approaches a beacon range is about 40m) and are not suitable for “always-on modes”. LTE Direct instead allows a device to discover nearby devices or services in 
approximately 500m proximity range, making apps continuously aware of device surroundings in a battery efficient manner; furthermore it aims to work also in crowded environments, discovering 1000s of devices/services\cite{lte_direct_qualcomm}.\\
Mobile applications can instruct LTE Direct to monitor for nearby services on other devices or to broadcast their own services through Expressions, filtering data at the device level, without the needs of a centralized architecture or network pings. Moreover applications don’t need to keep running in order to react to Expressions but they can be notified of them directly by the device.\\
LTE Direct-enabled devices broadcast local running services and requests through “Expression”: these are service-layer identifiers representing the offered or requested service, identity, interest or location. Other devices wake up periodically (e.g. every 10 seconds) listening for Expressions and broadcasting their own: for each Expression received, the latter is filtered on the basis of applications requests, and eventually notifies them of the proximity event.


\section{Platform overview}
\label{sec:platform_overview}
In this section we are going to analyze main features of some platforms offered by several companies operating in indoor-location and mobile fields together with some academic projects developed. The aim of this overview is to understand indoor-location services history and new trends in this field.\\
We tried to group platforms by technology used with the same order defined in previous subsection.

\subsection{Redpin}
\label{sub:redpin}
The Redpin project\cite{redpin} is an open source fingerprint-based indoor location system specifically designed to run on standard mobile devices like smartphones.\\
The aim of the project is to obtain room level location accuracy with a deployment easier than previous proposed solutions: in particular here they exploit final users collaboration for training phase.\\
For each room mapped they record more than one measurement: each fingerprint is the mapping between the room where measurement take place and the signal strength of the currently active GSM cell, the signal strength of all WiFi access points as well as the Bluetooth identifier of all non-portable Bluetooth devices in range.\\
The room level accuracy requested helped them to reduce measurement errors or fingerprint mismatch during application running, due to rooms' walls signals absorption.\\
Redpin consists of two components: a Sniffer that runs on a mobile device and collects signals and fingerprints and a Locator component, that runs on a separate server. The Sniffer scans for signal sources and create the fingerprint while the application code gets the fingerprint and queries the Locator through the network for a corresponding location. The server instead keeps track of all collected fingerprints and their map with real locations (in this case with rooms names) and sends back to the Sniffer query results produced by the location algorithm implemented.\\
The location algorithm tries to get a best match out of the given fingerprint: the algorithm uses a threshold value on the distance calculated as a similarity between identifiers and signal strengths; if the distance is not above the threshold the fingerprint is considered as ``not matched".\\
When Redpin application starts, it sniffs for GSM active cell, Wifi access points (recording respective signals strengths) and nearby Bluetooth non-portable devices' IDs: sniffed data are sent to a central server that tries to locate the device given all know fingerprints. The Redpin application continuously sniffs for aforementioned data and queries the server. When a fingerprint match is found it displays to the user the detected location. If no fingerprint in the database matches the given one, than the user can suggest its current position in order to allow the central server to store the new fingerprint in the database: this collaborative approach can simplify the training phase that in other projects could take also some hours for large environments.


\subsection{ActiveBat}
\label{sub:activebat}
ActiveBat\cite{active_bat_site} is an ultrasonic-based location system that uses the principle of trilateration.\\
System's main components are: Bats, which are ultrasound receivers, and a central controller that coordinates the Bats and the receiver chains. Bats are ultrasound transmitters attached to tracked objects having the size of a car key; they are identified by a unique 48bit code and are linked with the fixed location system infrastructure through a radio link. Receivers are placed on the ceiling in a square grid, 1.2m apart, and are connected by a high-speed serial network.\\
When a Bat is to be located, the controller instructs it to send a short pulse of ultrasound, and its times-of-flight to receivers is measured. A DSP board executes the location algorithm and sends results to the application (running on a PC).By finding the relative positions of two or more Bats attached to an object, they can calculate its orientation.\\
The main problem of the system is the large number of receivers required to be mounted on the environment ceiling, and their precise alignment.

\subsection{Cricket}
\label{sub:cricket}
The Cricket system\cite{cricket} is an indoor location system developed by MIT taken as reference in lots of academic projects on indoor-location: it has an architecture similar to modern Bluetooth beacons based systems.\\
Cricket consists of ``nodes", ad-hoc hardware platforms with a transceiver and a microcontroller onboard able to send signals over Radio frequency (RF) and ultrasonic band (US), and able to interface with a host device. There are two types of nodes: beacons that act as fixed reference points of the location system (usually attached to walls) and listeners attached to fixed or mobile objects that need to determine their location.\\
Beacons transmit their own location as coordinates, but when they are deployed they don't know their coordinates: to solve this issue a listener attached to a host roams in the environment computing the distance between all beacons; the host then computes, on the basis of several measurements, a unique inter-beacon distances set used to build a uniquely coordinates set that defines the real beacons topology. Finally the host computes coordinates for each beacon such that they resemble real disposition.\\
Beacons periodically transmit a short ultrasonic pulse followed by an RF message containing beacon's information like absolute location (in the form of aforementioned coordinates set), a unique beacon identifier, the physical space associated with the beacon, etc.\\
There's no central coordination of beacons transmissions. Each listener listens to beacon transmissions, measures its distance from nearby beacons and uses this information to compute its currently absolute position: the distances are measured using a TDoA approach (see TDoA description in section \ref{subsub:tdoa}) on RF and ultrasound signals (using the US pulse sent just before a beacon RF message). If the listener has multiple ultrasonic sensors it can use distance difference information to compute its orientation.\\
The aim of the system was to have high location accuracy and ease of deployment: the testing outcomes results in a 10cm position error with listeners fixed on a parallel plan with respect to beacons' plan.

\subsection{Navizon}
\label{sub:navizon}
Navizon\cite{navizon}\cite{navizon2} is a company that offers indoor-location services based on WiFi technologies. In particular they exploit smart WiFi nodes (similar to regular access points): each of them periodically detects WiFi-enabled devices such as smartphones, tablets, laptops or WiFi tags and records their MAC addresses and signal strength, pushing these data to a remote server hosted on a private or public cloud platform. The server estimates devices' locations and records them in local. Each device can query the server through a REST API in order to obtain its own location.\\
In this case hardware nodes have to be deployed and at least one node must be connected to the internet (nodes form an ad-hoc network in order to allow each of them to push data to the remote server). For a whatever small environment, the company states that at least 5 nodes have to be deployed.\\
An accuracy of 2-3m is claimed by the company with a 15-20m spacing between adjacent nodes.\\
The platform provides a Proximity Engine through which the system administrator can instruct the server to notify another ``listener server" (using a POST message) about proximity events fired: these events can be defined as signal strength thresholds on a specific node of a specific site.

\subsection{Insiteo}
\label{sub:insiteo}
Insiteo is a company that offers indoor-location services such as navigation, geofencing and indoor maps. They have a configuration platform through which devices can download setup data in order to run properly. Its solutions mix WiFi access points data with Bluetooth beacons (iBeacon compatible) and devices' sensors such as accelerometers or pedometers in order to obtain better performance (claimed 2m by company website).\\
The platform they sell allows analytics tasks based on location data collected by devices, indoor map loading and management and devices configuration.


\subsection{Allseen - Alljoyn}
\label{sub:alljoyn}
Allseen\cite{allseen_home} is an open world-wide consortium that is trying to develop a global standard for IoT communications called Alljoyn.\\
Alljoyn is a framework that provides an ad-hoc proximal network to several devices running on different platforms and operating systems through a distributed bus: in this way all devices connected to the same WiFi network (in the future other ``transport technologies" could be supported) can communicate between each others using the provided interface.\\
The Alljoyn power is its capability to adapt to lots of scenarios thanks to service discovery functionalities offered in order to make applications aware of devices or services available on the network: a developer can easily build its own service, defining the Actions other peers can perform on it and the signals it can send out (Events); in this way developers can instruct applications with if-this-then-that rules for smarter interactions, even application-to-application interactions.\\
All communications are performed by a local or remote router instantiated by the framework on behalf of the application layer: for example in case of embedded devices with low computational capabilities the router used is a remote one that resides on a more powerful peer in the network (see figure \ref{fig:alljoyn_network}).


	\begin{figure}[]
		\centering{\includegraphics[width=\textwidth]{images/state_art/alljoyn_architecture}}
		\caption{Alljoyn network architecture}
		\small source: \url{https://allseenalliance.org}

		\label{fig:alljoyn_network}
	\end{figure}

The communication can happen in different formats like synchronous messages, asynchronous signals (used for notification purposes), etc.\\
As mentioned before the Alljoyn project is growing more and more with several big companies interested in it. Due to the wide range of applications such a standard can have, some projects were started as ``Alljoyn subprojects", developed by approved working groups, in order to implement some services useful to lots of applications: as an example a notification service has been developed in order to send text, audio or images notifications to peers in the network, a configuration service that allows peers to set a property of another peer through the network, etc. Between these projects there's a Location Service Project\cite{allseen_location}. It introduces the concept of Entity as a general item applications in the network are interested in and it consists of four different services we briefly describe here:

\begin{itemize}
	\item \textbf{Presence}: this service announces the entry or exit of an entity to the proximal network. Network peers can query for the presence of an entity in the network or they can subscribe to entry or exit events
	\item \textbf{Proximity}: this service provides proximity data on the relative distance between an entity and a proximity service host. A peer can query for a distance relative to an entity or subscribe to distance updates with minimum update interval or proximity variation constraints
	\item \textbf{Location}: this service provides absolute location data about entities both in a synchronous or asynchronous fashion (always with interval and location updates constraints)
	\item \textbf{Containment}: this service allows peers to query for the containment relationship between two entities. For example a peer can perform a query to know whether an entity A is contained by an entity B or vice versa
\end{itemize}


\subsection{Google Nearby}
\label{sub:nearby}
Nearby\cite{google_nearby} is a Google project that provides a Proximity API for iOS and Android developers; thanks to this API, Android and iOS devices can discover and communicate with each other, as well as with beacons. \\
In proximity field, Nearby uses a combination of WiFi, Bluetooth and inaudible sound (using the device’s speaker and microphone) to establish proximity and to communicate with devices in proximity.\\
The ``handshake" between two devices in proximity is possible using Google Cloud Services for authentication purposes: this doesn't means that only Google users can use the service, but the process transparently uses random generated tokens stored in the cloud to perform a kind of challenge-response mechanism and notify two involved devices of their proximity (without a distance estimate).\\
The programmer can instruct the APIs with an indirect range setup: in fact he can limit the ``scanning range" to Bluetooth and ultrasound or to ultrasound only range.\\
To communicate, Nearby exposes publish-subscribe methods, meaning that an app publishes a payload that can be received by subscribers in proximity. So, it is possible to build applications for:
\begin{itemize}
	\item messages sharing (using Nearby Messages API): it is useful for interactions such as resource sharing or collaborative editing
	\item real-time connections between devices (using Nearby Connections API): it is useful for local multiplayer gaming and multi-screen gaming.
\end{itemize} 



\section{Conclusions}
In this chapter we analyzed currently available solutions for location-aware and proximity-aware applications. As you can notice the focus is more on providing platforms location-oriented than proximity-oriented ones. We think the main motivation is the higher flexibility provided by location data: in fact proximity data can be inferred by location data these systems provide, motivating companies interests to develop location-oriented products for marketing reasons. 
\\
Furthermore almost all systems examined exploit centralized architectures and cloud features to provide smart interaction modes and data analysis or aggregation features not always needed. 
\\
As reported in previous sections, last years saw an always growing interest in location-aware scenarios from different points of view: providing new smart interaction modes and context-aware information quickly became a must for successful applications in whatever field, from ``simple" smartphone applications to data collect tasks and software.\\
Nowadays we are seeing this interest moving from technological development (protocols, sensors and technologies) to platforms and smart services offered (see section \ref{sub:nearby}).






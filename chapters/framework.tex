


								%------------------------------------------------------------
								%					Problem analysis
								%------------------------------------------------------------


The ADPF framework aims to define common abstractions suitable for proximity-aware applications, ensuring at the same time good flexibility and performance for current and future application fields.\\
Its focus is to provide new high level objects representing all kinds of actors involved, define a unique data representation suitable for different technologies, and hide low level details to more easily face future evolutions from a technological point of view.

In section \ref{sec:problem_analysis} we provide a general overview of the problems addressed by ADPF.
Section \ref{sec:use_cases} presents some general use cases pointing out key features and real world scenarios where ADPF could be deployed.\\
Sections \ref{sec:functional_requirements} and \ref{sec:non_functional_requirements} define functional and non-functional requirements as inferred from the use cases we present.\\
Section \ref{sec:framework_abstr} gives a high level description of the abstractions used by the framework together with the framework's main APIs which are explained in section \ref{sec:framework_api}.\\
Sections \ref{sec:distr_approach} and \ref{sec:communication} describe ADPF's architectural choices, while section \ref{sec:security} focuses on security and privacy aspects.\\
Finally section \ref{sec:framework_work} illustrates ADPF's working principles in order to give the reader an overall description of the idea behind framework's implementation.



\section{Problem analysis}
\label{sec:problem_analysis}
%
%
%------------------------------------------------------------
%					Importanza della proximity rispetto alla location
%------------------------------------------------------------
%
Lots of location-aware applications, especially in those apps that use indoor-location data, need to know the relative distance between the device they’re running on and other devices or application specific ``actors": for example, in the case of proximity marketing or couponing an app needs to know when the user entered a shop in order to provide products offers, or custom advices.\\
The key information used by the apps’ business logic is the \textbf{proximity} between these actors.\\
Nowadays location or indoor proximity information are mainly made available using a specific technology: for example, a shop can develop its own mobile app that uses the bluetooth beacons or WiFi based devices deployed therein for indoor-location purposes. Each of these technologies has its own library the programmer has to understand and integrate inside his app, taking into account all aspects not managed by the library itself. But what if an application scenario involves more than one technology?\\
%
%------------------------------------------------------------
%					Motivo per i POI
%------------------------------------------------------------
%
Although our work is mainly focused on indoor environments, we also defined a ``bridge" with outdoor technologies (exploiting the well-known macro-location facilities that are offered off-the-shelf by the development platform) so that the programmer can maintain continuous control over user context transitions.\\
%
%------------------------------------------------------------
%		Motivazione per device-to-device, subscriptions e utilizzo nostro framework
%------------------------------------------------------------
%
Section \ref{chap:state_art} pointed out some interesting facts about the indoor-location field: the recent development and release of new standards and technologies (see Google Eddystone in section \ref{sec:platform_overview}), the importance of device-to-device proximity awareness (see Google Nearby and LTE-Direct in sections \ref{sub:nearby} and \ref{sub:tech_lte}) and the agreement on event-based communication between location services and applications.
%
%------------------------------------------------------------
%				Motivazione per gruppi
%------------------------------------------------------------
%
Furthermore, we thought it could be useful for a programmer to have device and object grouping functionalities.\\
%
%------------------------------------------------------------
%					Conclusione
%------------------------------------------------------------
%
As a result our framework takes into account all mentioned aspects and tries to face related problems providing straightforward approaches, as easy to use and as safe as possible. A distributed approach to the problem reduces technology constraints for ADPF-powered applications, limiting costs and effort for their deployment.\\
The following sections explain all these features in detail, through some toy examples and application scenarios.


									%
									%------------------------------------------------------------
									%					General use cases
									%------------------------------------------------------------
									%
\section{Use cases}
\label{sec:use_cases}
In this section we are going to define some ``general use cases" that encapsulate several scenarios and examples: they are not meant to show a particular application for indoor proximity but to motivate our framework's functionalities in different situations.


%
%------------------------------------------------------------
%					Use case 1
%------------------------------------------------------------
%
\subsubsection{Proximity notifications}
\label{sub:proximity_notifications}
Use cases belonging to this class are maybe the most known and most common because they represent the ``101 toy examples" about indoor proximity and BLE beacons: retail stores, cinemas and stations are only a few of the contexts for these kinds of examples; couponing and targeted advices are the most valuable outcomes.\\
We would like to have the same functionalities in other situations, where proximity driven notifications can be powerful. Some examples might be the delivery of custom evacuation instructions to people in a building, in the case of emergency, depending on their current floor or room; the delivery of tracking notifications to warehouse employees; and queue waiting time estimates in public offices reported upon user arrival.\\
Besides this, in some situations it could be useful to automatically deal with surrounding changes: for example, a user can ask to be notified about the proximity to entities that have specific properties that evolve over time (e.g. ``tell me when I'm near a vending machine that was refilled less than 2 days ago").\\


%
%------------------------------------------------------------
%					Use case 2
%------------------------------------------------------------
%
\subsubsection{Device-to-device proximity awareness}
\label{sub:d2d_proximity}
In the last few years social networks have been enriched with smarter functionalities. Some exploits user context data, from a post's location to photo geotagging and event tagging of whatever content a user publishes.\\
Several academic projects and companies have started analyzing possible solutions to provide user proximity information in a reliable manner (see section \ref{sub:nearby}): in the case of social networks, matching profiles or friends in a user's surroundings can be exploited to provide ``immersive" interaction modalities. For example, in a job fair users might want to be aware of whether they are near a company that has open positions matching their skills, or a company might want to be notified about the proximity of new candidates with certain knowledge.\\
In the smart ticketing field (for events or public transport) a ticket controller could use an app to discover people nearby without a valid ticket in a quick and easy manner, even in crowded situations.
Furthermore, virtual leashes can easily be set in order to track pets or children (so that we are notified when they leave a defined space - e.g. a mart playground or a park).

%
%------------------------------------------------------------
%					Use case 3
%------------------------------------------------------------
%
\subsubsection{Proximity triggers}
\label{sub:proximity_triggers}
In the growing IoT and smart home markets (see section \ref{sub:alljoyn}), the chance to have smart appliances take actions on the basis of simple ``if-this-then-that" rules seems almost a basic requirement. The machine-to-machine communication that triggers these actions actually give smart products a competitive edge on the market.\\
The integration of proximity information as a triggering conditions can be useful in lots of cases: to enable stove security when children are nearby, to turn on the lights when someone enters in the house, to have an appliance emit a ``task finished" sound only if somebody is in its surroundings, and so on.\\
Triggers can be used to manage and generate whatever type of data. For example, they can be used to detect people nearby and collect statistics on the basis of their properties. This kind of application can be useful for trends discovery and data forecasting.


					%
					%------------------------------------------------------------
					%					Functional Requirements
					%------------------------------------------------------------
					%

\section{Functional requirements}
\label{sec:functional_requirements}

In this section we are going to define the functional requirements of the framework, as identified from the use cases presented in section \ref{sec:use_cases}.

\begin{itemize}
	\item \textit{Common abstract entity}\\
	In order to keep our framework as flexible as possible, we use an abstract entity (with common description fields) to map each real world actor involved in an application. This way the framework can access all the actors through the same interface, distinguishing them only by framework relevant information.

	\item \textit{Device profile}\\
	Each device in the network has to maintain a description so that it can be identified in device-to-device proximity discovery, as well as advertise it in framework level communications.
	
	\item \textit{Proximity data}\\
	The framework has to provide proximity information to devices in the network, in terms of presence and relative distance.

	\item \textit{Always-running}\\
	The framework has to offer a wake up mechanism in order to react to proximity/location events even when the client app using it is in the background or stopped. An activation mechanism is needed so that energy consumption doesn't waste the devices' batteries.

	\item \textit{Notification mechanism}\\
	In the real world proximity events happen with no standard frequency or predefined instants. The framework has to deal with both synchronous and asynchronous events based on the actors' request types. For example a request could be to ``give me all the entities within proximity" (synchronous) or ``tell me when I arrive at my office" (asynchronous).

	\item \textit{Grouping}\\
	The framework has to offer the possibility to choose interesting entities so that it only receives relevant proximity events.
\end{itemize}


	%
					%------------------------------------------------------------
					%					Non-functional Requirements
					%------------------------------------------------------------
					%

\section{Non-functional requirements} 
\label{sec:non_functional_requirements}
We identified two main non-functional requirements: first of all, since we can connect devices and broadcast potentially sensible profile data, the framework has to allow the user to decide what and when to share information. Furthermore, the programmer should be able to encrypt communications if activated by the application's context.
Secondly, we need to hide the complexities of proximity technologies so that future solutions can be easily integrated in the framework, and to lower the effort required of a programmer to manage his/her entities.



\section{Framework abstractions}
\label{sec:framework_abstr}

In this section we present the framework components we identified to abstract the above requirements. For the technical detail please refer to Chapter \ref{chap:library}.

\subsection{Entity}
\label{sub:entity}

To fulfill the requirement of having a common descriptor for every actor, we defined a general component \entity{}. An \entity{} is represented by four fields:

\begin{enumerate}
	\item \textit{entityID} \\
	This attribute uniquely identifies an \entity{}. It is not possible to define two entities with the same \texttt{entityID}.
	\item \textit{entityType} \\
	This field is used to distinguish entities by their type. We currently support two types: \texttt{DEVICE} and \texttt{BEACON}. The framework offers the possibility to easily extend the list of possible types (see section \ref{sub:utility_classes} for more details).
	\item \textit{distanceRange} \\
	This represents a relative distance for the \entity{}. See section \ref{sub:distanceRange} for more details about \texttt{DistanceRange}.
	\item \textit{properties} \\
	This attribute is used to describe the properties that are typical of each \entity{}. These are only relevant from an application's point of view. For example, the developer of an app for sports centers will add the fact that a user is interested in basket.
\end{enumerate}


\subsection{SelfEntity}
\label{sub:self_entity}

\texttt{SelfEntity} is a particular \entity{} built by the framework; it describes the device where the framework is running. It is used to address the need of a device profile, in order for the entity to be identified during framework communication (see section \ref{sec:functional_requirements}).



\subsection{DistanceRange}
\label{sub:distanceRange}

To deal with the current technological limits in terms of distance computation accuracy\cite{Dahlgren2014}, we decided to use a set of relative distances, each one representing a range of meters. In particular, we identified these distance ranges: 

\begin{table}[H]
\centering
\begin{tabular}{ll}
\toprule
DistanceRange              &        Description                                   \\ \midrule
IMMEDIATE                  &         0m - 0.5m                                    \\
\addlinespace[0.5em]
NEXT\_TO                    &        0.5m - 2m                                     \\
\addlinespace[0.5em]
NEAR                       &         2m - 4m                                       \\
\addlinespace[0.5em]
FAR                        &         4m - 8m                                        \\
\addlinespace[0.5em]
REMOTE                     &         more than 8m                                   \\
\addlinespace[0.5em]
SAME\_BEACON                &         entities are seeing the same beacon            \\ 
\addlinespace[0.5em]
SAME\_WIFI                  &         entities are in the same network               \\
\addlinespace[0.5em]
UNKNOWN                    &         no distance information available              \\ \bottomrule
\end{tabular}
\caption{DistanceRange table}
\label{tab:distance_range_table}
\end{table}

Latest three values reported in the table above are returned when there is no useful information to estimate relative distance between entities: SAME\_BEACON means that two entities (A and B) detect the same BLE beacon but it's impossible to compute their proximity, SAME\_WIFI instead is used when A and B have detected no common BLE beacons but they are in the same environment (meaning that they can communicate). Eventually, UNKNOWN is used when there is not distance information at all.

\subsection{POI}
\label{sub:poi}

A \poi{}, acronym for Point Of Interest, represents a real world location where there are entities that are deemed useful for the application. In practice this means that a \poi{} identifies a location where there are beacons.\\
It is described by a name, a latitude, a longitude and a radius so that a geofence can be created, and a beaconUuid that is used to identify the beacons of interest.
The idea is that a BLE scan (and Bluetooth activation) only occurs after the entry in the geofence. This way, the framework does not waste the device's battery scanning for BLE beacons when it is useless.


\subsection{Group}
\label{sub:group}

A \group{} is a set of entities that have something in common: to define it a programmer must provide an ``entity prototype" that all group members have to match.\\
This prototype is described by:

\begin{itemize}
	\item an entityID: if this field is defined, the group can be composed only by zero or one entity.
	\item an entityType: with this field, the programmer can decide to create a group based on entity type; the type can be \texttt{DEVICE}, \texttt{BEACON} or \texttt{ALL} (to select both devices and beacons).
	\item a propertiesFilter: through this attribute the programmer can define a filter on \entity{} properties. Thanks to propertiesFilter it is possible, for example, to create a group with all the entities that are interested in ``western movies".
\end{itemize}

It is important to notice that a \group{} only defines which entities are part of it. There's no direct binding between a group and its entities (i.e. a group does not ``physically" collect entities that belong to it). This is due to the fact that the \entity{} set changes continuously. Entities can change their properties, or they can enter(exit) in(from) the \poi{}. Keeping an always updated list of entities for each group can be a time and energy consuming task, definitely infeasible in terms of memory consumption and energy expensive synchronization tasks.



\subsection{Events}
\label{sub:events}

Here, we define events in a more fine-grained manner. \\
In particular, the framework identifies three different types of events:

\begin{enumerate}
	\item \textit{Group events} \\
	These are all the events that are related to group changes. They are produced when an entity changes its properties, or when an entity enters or leaves a \poi{}. 

	\item \textit{Proximity events} \\
	These are the events that are strictly related to proximity. Every time an entity is in proximity to another, whether device or beacon, a proximity event is fired.

	\item \textit{Geofence events} \\
	These are events that are fired when a geofence entry/exit happens. The geofence can be built around a beacon, or around a single device.
\end{enumerate}




\section{Framework APIs}
\label{sec:framework_api}

In this section we explain how the framework interacts with the programmer. We explain the framework's APIs from a conceptual point of view. For technical details, please refer to chapter \ref{chap:library}. \\
Given that there are three different types of events (see section \ref{sub:events}), the framework offers the programmer three different types of subscription:

\begin{enumerate}
	\item \textit{Group subscription} \\
	This is the subscription a programmer has to do to be notified about group events. A group subscription requires only one parameter, the \group{} G the programmer is interested in to receive notifications: every time an entity enters(exits) in(from) G, a group event for that subscription is fired.

	\item \textit{Proximity subscription} \\
	This is the subscription related to proximity events. The programmer has to pass an \entity{} E (which can be only selfEntity - see \ref{sub:self_entity} - or a beacon), and a \group{} G: every time an entity belonging to G is in proximity with respect to E, a proximity event for that subscription is fired.

	\item \textit{Geofence subscription} \\
	This represents the subscription for geofence events. In this case, the parameters required are an \entity{} E, a \group{} G and a \distanceRange{} D; the framework builds a geofence of radius D around E: every time an entity belonging to G enters/exits in the geofence, a geofence event for that subscription is fired.
\end{enumerate}

The three aforementioned subscriptions are obviously asynchronous: the programmer subscribes for an event and, at a certain point, he will be notified of the firing of the latter. \\
ADPF offers also a synchronous method, to retrieve all the entities (eventually belonging to a \group{}) in proximity with respect itself in a certain instant of time. This could be useful for the programmer to have an overview of the application state in a certain moment, or to retrieve entities of interest. \\ 
Finally, the framework provides obviously a function that permits the programmer to unsubscribe from previously made subscriptions.


\section{The distributed approach}
\label{sec:distr_approach}

We decided to store all the business logic needed to compute proximity, fire events and so on, on client side, i.e. on devices, without the help of a centralized server. This distributed approach differentiates ADPF from the totality of solutions currently available on the market (which are explained in section \ref{sec:platform_overview}). We opted for a distributed solution in order to reduce deployment costs and physical hardware needed for developers who are going to use our framework for their applications.


\section{Communication}
\label{sec:communication}

As mentioned in non-functional requirements (see section \ref{sec:non_functional_requirements}) we needed a way to permit communication among devices: we decided to use WiFi, as it is supported by almost all Android devices on the market and, due to its broad diffusion, makes sense the hypothesis of its presence in most of real use cases (furthermore, whenever it is absent should be easy to deploy). \\
Communication happens through well-defined messages (see section \ref{sub:framework_messages}) using the publish-subscribe pattern, so that the centralized broker is very lightweight and can be deployed also on cheap machines, in accordance with the choice of a distributed approach (as said above in section \ref{sec:distr_approach}).\\
A high level network schema is reported in Figure \ref{fig:framework_net_arch}: devices connects to the broker in order to be able to send and receive framework related messages and infer proximity information about different actors in the \poi{}.


\begin{figure}[H]
	\makebox[\textwidth][c]{\includegraphics[width=0.5\textwidth]{images/framework/network_architecture}}%
	\caption{Framework network schema}
	\label{fig:framework_net_arch}
\end{figure}


\section{Security}
\label{sec:security}

To address the problem of sharing potentially sensible information, we introduced a security module that permits to the user to select what can be sent on the network by the framework, and what cannot. In particular, a user can decide (only before framework starting, in configuration phase) to send:

\begin{itemize}
	\item only messages related to proximity and geofence events;
	\item only messages related to group events;
	\item no messages.
\end{itemize}

Furthermore a user can decide to switch on/off the ``Ghost Mode" at runtime: enabling Ghost Mode, the device becomes invisible from other users point of view.

Finally, in configuration phase it is also possible to decide whether to send messages using SSL or not.




\section{Framework working principles}
\label{sec:framework_work}

In the previous sections we defined all components and concepts needed to understand how the framework works from a conceptual point of view. So, in this section we are going to explain how ADPF interacts with programmers and applications that use it.

\subsubsection{Initial configuration and start}
\label{subsub:init_config}

The first and fundamental thing a developer has to do, is to give to ADPF a list of \poi{}s (see section \ref{sub:poi}); in this way, the framework is able to know when turning on Bluetooth and starting BLE beacons ranging. 
Furthermore, the framework needs a security configuration (see section \ref{sec:security}) in order to know what messages it can send, and in which way (i.e. using or not SSL). \\
At this point, the framework is ready to run: when the application is started, ADPF starts to retrieve position using GPS and Android location functionalities, in order to find out when the user enters in one of \poi{}s' geofences.


\subsubsection{Network entry}
\label{subsub:network_entry}

After a \poi{}'s geofence entry, ADPF turns on device's Bluetooth, and begins to scan for BLE beacons with UUID defined for that \poi{}. 
At the same time, it notifies the client app about the entry event and the app itself can perform an attempt to connect to the broker. If the attempt goes fine, the framework configures the network module for publish-subscribe (connection to broker, subscription to topics, etc.) and announces himself to other entities publishing a message (the \checkinmsg{} message) of network entry: in this way, entities interested in group changes on groups selfEntity (see section \ref{sub:self_entity}) belongs to receive a notification of check in (conceptually equal to a group join). 
Instead, in case the connection attempt goes wrong, the client app is notified about it and could for example retry a connection. \\
Upon the connection to the broker is established, the user can subscribe to events is interested in: for every subscription made, ADPF returns to programmer a corresponding object \texttt{Subscription} (see technical details in section \ref{subsub:impl_subscription}), on which he can register a callback function that has to be executed once the associated event is fired. 


\subsubsection{BLE scanning and communication}
\label{subsub:ble_scan}

Up to this point, ADPF is connected to network and is scanning for BLE beacons. Every time it scans, it updates the list of last beacons seen and verifies if some of them matches some proximity or geofence subscription: for every match, it fires the corresponding event. \\
Furthermore, after a fixed number of scans, the framework broadcasts a message (\proxbcnmsg{} message) with the list of last detected beacons and the computed \distanceRange{} (see section \ref{sub:distanceRange}) for each of them, that is used by other devices to find out if they are in proximity.


\subsubsection{Proximity computation}
\label{subsub:prox_computation}

When a \proxbcnmsg{} message is received from another device, ADPF compares message beacons with the last detected ones by selfEntity: if at least one is in common, and corresponding \distanceRange{}s are useful (see section \ref{subsub:tech_proximity_computation}), the framework is able to say that selfEntity and the message sender are in proximity: so, it sends to message sender a \proxupdmsg{} message, containing information about that computed proximity. \\
If instead ADPF has sent a \proxbcnmsg{} message and has received a corresponding \proxupdmsg{} message by someone, let's say \entity{} E, it checks if there's some proximity or geofence subscription between selfEntity and a group containing E, and for each matching subscription it fires the corresponding event. \\
In figure \ref{fig:prox_comp} is shown proximity computation with a simplified schema, in order to better understand the aforementioned process.

\begin{figure}[H]
	\makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{images/framework/proximity_event}}%
	\caption{Proximity computation}
	\label{fig:prox_comp}
\end{figure}



\subsubsection{Properties update}
\label{subsub:prop_update}

At a certain point, an \entity{} can decide to update its properties (see section \ref{sub:entity}); in this case, framework has to communicate this change to other entities, because an update of properties could mean a group leave/join. For example, if a user wants to change his interests from ``comic movies" to ``action movies", he is leaving the \group{} defined by entities interested in comic movies, and joining the \group{} defined by entities interested in action movies. \\
Then, a properties change is followed by a \propupdmsg{} message sending, containing selfEntity information, old and new properties. When a similar message is received, ADPF checks if there is some group subscription with a \group{} matching old or new properties, and in that case it fires the corresponding group join/leave event.



\subsubsection{Synchronous proximity request}
\label{subsub:sync_req}

As said in section \ref{sec:framework_api}, a programmer has the possibility to execute a synchronous request to retrieve all entities belonging to a \group{} G in proximity (i.e. with a \distanceRange{} less or equal to the defined one in the request). Also this operation happens in a distributed way: the \entity{} interested broadcasts a message (\syncreqmsg{} message) containing all information about the request (the \group{}, the \distanceRange{}, the last seen beacons). Every device receives this message, checks if it matches the \group{}, and in this case computes its proximity with respect to that entity: if they are in proximity and the computed \distanceRange{} is less or equal to the requested one, the device sends to the entity who sent the request a \syncrespmsg{} message containing information about himself and the \distanceRange{} obtained.\\
At this point entities already filtered (on group and distance) are ready to use.


\subsubsection{Ghost Mode}
\label{subsub:ghost_mode}

As explained in section \ref{sec:security} the user has the possibility to switch on/off the Ghost Mode at runtime: when Ghost Mode is enabled, ADPF simulates a network disconnection (broadcasting an appropriate \checkoutmsg{} message) and stops sending any kind of message. In this way, the device becomes ``invisible", but continues to receive messages from other entities. The user can than decides in every moment to turn back ``visible" disabling Ghost Mode: in this case the framework simulates a network entry (with a \checkinmsg{} message) and restart sending messages, according with security configuration set on startup.


\subsubsection{Network exit}
\label{subsub:network_exit}

When a device exits from the network (for whatever reason, from \poi{} exit to network error) ADPF broadcasts a \checkoutmsg{} message, containing information about selfEntity. Entities receiving the message check for matching group subscriptions (i.e. the \entity{} that sent the \checkoutmsg{} message belongs to the \group{} they subscribed for), and for each match they fire a check out event (conceptually equal to a group leave).
